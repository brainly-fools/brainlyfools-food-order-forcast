from matplotlib import pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error, median_absolute_error, explained_variance_score, max_error
import pandas as pd
import numpy as np
import pickle


def Model(id, week, center_id, meal_id, checkout_price, base_price, email,	homepage, city_code, region_code, op_area, beverages, biryani, Desert, Extras, Fish, other_snacks, pasta, pizza, rice, salad, sandwich, seafood, soup, starter, continental, indian, italian, thai, typeA, typeB, typeC):
    filename = 'finalized_model.sav'
    loaded_model = pickle.load(open(filename, 'rb'))
    array = [[id, week, center_id, meal_id, checkout_price, base_price, email,	homepage, city_code, region_code, op_area, beverages, biryani,
             Desert, Extras, Fish, other_snacks, pasta, pizza, rice, salad, sandwich, seafood, soup, starter, continental, indian, italian, thai, typeA, typeB, typeC]]
    result = loaded_model.predict(array)
    return result[0]
