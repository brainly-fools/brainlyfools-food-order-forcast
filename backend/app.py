import re
from flask import Flask, render_template
from flask.wrappers import Request
from flask import request
from model import Model

app = Flask(__name__)


def Beverages(food):
    if food == "Beverages":
        return 1
    else:
        return 0


def Biryani(food):
    if food == "Biryani":
        return 1
    else:
        return 0


def Desert(food):
    if food == "Desert":
        return 1
    else:
        return 0


def Extras(food):
    if food == "Extras":
        return 1
    else:
        return 0


def Fish(food):
    if food == "Fish":
        return 1
    else:
        return 0


def Pasta(food):
    if food == "Pasta":
        return 1
    else:
        return 0


def Pizza(food):
    if food == "Pizza":
        return 1
    else:
        return 0


def Rice_Bowl(food):
    if food == "Rice Bowl":
        return 1
    else:
        return 0


def Salad(food):
    if food == "Salad":
        return 1
    else:
        return 0


def Soup(food):
    if food == "Soup":
        return 1
    else:
        return 0


def Starter(food):
    if food == "Starter":
        return 1
    else:
        return 0


def Continents(food):
    if food == "Continental":
        return 1
    else:
        return 0


def Indian(food):
    if food == "Indian":
        return 1
    else:
        return 0


def Italian(food):
    if food == "Italian":
        return 1
    else:
        return 0


def Thai(food):
    if food == "Thai":
        return 1
    else:
        return 0


def Center_TypeA(center):
    if center == "Type A":
        return 1
    else:
        return 0


def Center_TypeB(center):
    if center == "Type B":
        return 1
    else:
        return 0


def Center_TypeC(center):
    if center == "Type C":
        return 1
    else:
        return 0


def op_area(operation):
    if operation == "New York":
        return 1
    if operation == "washington":
        return 2
    if operation == "Virginia":
        return 0


def email(medium):
    if medium == "email":
        return 1
    else:
        return 0


def Homepage(medium):
    if medium == "Homepage":
        return 1
    else:
        return 0


@app.route("/", methods=["POST", "GET"])
def intial_func():
    prediction = 0
    if request.method == "POST":

        food = request.form["food"]
        operation = request.form["operation"]
        center = request.form["Center-Type"]
        medium = request.form["medium"]
        base = request.form["base"]
        checkout = request.form["checkout"]
        week = request.form["week"]
        city = request.form["city"]
        region = request.form["region"]
        prediction = Model(102832, week, 85, 1885, checkout, base, email(medium), Homepage(medium), city, region, op_area(operation), Beverages(food), Biryani(food), Desert(food), Extras(food), Fish(food), 0, Pasta(
            food), Pizza(food), Rice_Bowl(food), Salad(food), 0, 0, Soup(food), Starter(food), Continents(food), Indian(food), Italian(food), Thai(food), Center_TypeA(center), Center_TypeB(center), Center_TypeC(center))

    return render_template("index.html", predict=prediction)


if __name__ == "__main__":
    app.run(debug=True)
